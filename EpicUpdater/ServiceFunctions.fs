module ServiceFunctions

open System.Threading
open System.IO
open System.IO.Compression
open ICSharpCode.SharpZipLib.GZip
open ICSharpCode.SharpZipLib.Tar
open DnsClient
open FSharp.Data

let minutesInterval = 1

let getLatestVersionNumberAsync package =
    let lookup = LookupClient()
    async {
        let! result = lookup.QueryAsync
                          (package + "version.yurialbuquerque.dev",
                           QueryType.TXT) |> Async.AwaitTask
        let record = result.Answers.Item 0 :?> Protocol.TxtRecord
        return record.Text |> Seq.item 0
    }

let getFileName package version =
    let fileNameBase = package + "_" + version + "_amd64"
#if Windows
    let fileName = fileNameBase + ".zip"
#else
    let fileName = fileNameBase + ".tar.gz"
#endif

    fileName

let downloadFileAsync fileName downloadDir =
    let url = "https://d1ftoepmu0es39.cloudfront.net/" + fileName
    let downloadPath = Path.Combine(downloadDir, fileName)
    async {
        printfn "Downloading %s" downloadPath
        // `use` calls dispose once the variable goes out of scope, which closes the filestream
        use streamDest =
            new FileStream(downloadPath, FileMode.Create, FileAccess.Write,
                           FileShare.Write, 0x1000, true)
        let! response = Http.AsyncRequestStream(url)
        use streamOrig = response.ResponseStream
        streamOrig.CopyTo streamDest
        printfn "Downloaded %s" downloadPath
    }

exception FileCorruptedException of string

let getSumAsync downloadPath =
    async {
        let! fileText = File.ReadAllTextAsync(downloadPath) |> Async.AwaitTask
        let matches =
            System.Text.RegularExpressions.Regex.Match
                (fileText, "^([a-fA-F0-9]{64}).*$")
        if not (Seq.isEmpty matches.Groups) then return matches.Groups.[1].Value
        else
            File.Delete downloadPath
            return raise (FileCorruptedException "sum couldn't be downloaded") // by throwing here, the system will try to download the checksum again next iteration
    }

let verifyFileSum downloadPath (checksum : string) =
    let fileStream = File.OpenRead downloadPath
    use sha256 = System.Security.Cryptography.SHA256.Create()

    let hash =
        sha256.ComputeHash fileStream
        |> Seq.map (sprintf "%02x")
        |> String.concat ""
    if hash.ToLower() <> checksum.ToLower() then
        printfn "Wrong sum.\nExpected: %s\nActual:   %s" (hash.ToLower())
            (checksum.ToLower())
        File.Delete downloadPath
        raise (FileCorruptedException "wrong sum") // by throwing here, the system will try to download the package again next iteration

let receiveKeys keys =
#if Windows
    let configFileName = Path.Combine("GnuPG", "gpg.exe")
    let executablePath = System.Reflection.Assembly.GetEntryAssembly().Location |> Path.GetDirectoryName
    let gpgExecFileName = Path.Combine(executablePath, configFileName)
#else
    let gpgExecFileName = "gpg"
#endif
    Array.iter (fun key ->
        let startInfo =
            System.Diagnostics.ProcessStartInfo
                (FileName = gpgExecFileName, Arguments = "--keyserver keyserver.ubuntu.com --recv-key " + key,
                 UseShellExecute = false, RedirectStandardOutput = true,
                 CreateNoWindow = true)
        use pProcess = new System.Diagnostics.Process(StartInfo = startInfo)
        pProcess.Start() |> ignore
        pProcess.WaitForExit()
        if pProcess.ExitCode <> 0 then
            raise (FileCorruptedException "could not download gpg keys")
    ) keys


let verifyFileSignature downloadPath gpgPath =
#if Windows
    let configFileName = Path.Combine("GnuPG", "gpg.exe")
    let executablePath = System.Reflection.Assembly.GetEntryAssembly().Location |> Path.GetDirectoryName
    let gpgExecFileName = Path.Combine(executablePath, configFileName)
#else
    let gpgExecFileName = "gpg"
#endif

    let startInfo =
        System.Diagnostics.ProcessStartInfo
            (FileName = gpgExecFileName, Arguments = "--verify " + gpgPath,
             UseShellExecute = false, RedirectStandardOutput = true,
             CreateNoWindow = true)
    use pProcess = new System.Diagnostics.Process(StartInfo = startInfo)
    pProcess.Start() |> ignore
    pProcess.WaitForExit()
    if pProcess.ExitCode <> 0 then
        File.Delete gpgPath
        File.Delete downloadPath
        raise (FileCorruptedException "wrong signature")

let downloadSumAsync package version downloadDir =
    let fileName = (getFileName package version) + ".sum"
    let downloadPath = Path.Combine(downloadDir, fileName)
    if File.Exists downloadPath then getSumAsync downloadPath
    else
        async {
            do! downloadFileAsync fileName downloadDir
            return! getSumAsync downloadPath
        }

let downloadPackageAsync package version downloadDir checksum =
    let fileName = getFileName package version
    let downloadPath = Path.Combine(downloadDir, fileName)
    async {
        if not (File.Exists downloadPath) then
            do! downloadFileAsync fileName downloadDir
        verifyFileSum downloadPath checksum
        return downloadPath
    }

let downloadSigAsync package version downloadDir =
    let fileName = (getFileName package version) + ".sig"
    let downloadPath = Path.Combine(downloadDir, fileName)
    async {
        if not (File.Exists downloadPath) then
            do! downloadFileAsync fileName downloadDir
    }

let zipDecompress package downloadDir (file : FileInfo) =
    let currentFileName = file.FullName
    let newFileName = Path.Combine(downloadDir, package)
    ZipFile.ExtractToDirectory(currentFileName, newFileName)

let tgzipDecompress downloadDir package (file : FileInfo) =
    use originalFileStream = file.OpenRead()
    let currentFileName = file.FullName
    let newFileName = Path.Combine(downloadDir, package)
    use gzipStream = new GZipInputStream(originalFileStream)
    use tarArchive = TarArchive.CreateInputTarArchive(gzipStream)
    tarArchive.ExtractContents(newFileName)

let extractPackage package version downloadDir =
    let fileName = getFileName package version
    let downloadPath = Path.Combine(downloadDir, fileName)
    let file = FileInfo(downloadPath)
#if Windows
    zipDecompress package downloadDir file
#else
    tgzipDecompress downloadDir package file
#endif

#if Windows

let home =
    System.Reflection.Assembly.GetEntryAssembly().Location
    |> Path.GetDirectoryName
    |> (fun x -> Path.Combine(x, "epic-files"))
#else
let home =
    Path.Combine("/var/lib/epic", "epic-files")
#endif


let downloadDir = home
let onStop() = printfn "Stopping"

let processPackage keys package =
    printfn "Processing package %s" package
    async {
        receiveKeys keys
        if not (File.Exists home) then Directory.CreateDirectory home |> ignore
        let! version = getLatestVersionNumberAsync package
        printfn "%s version: %O" package version
        let! checksum = downloadSumAsync package version downloadDir
        let! packageFilename = downloadPackageAsync package version downloadDir
                                   checksum
        do! downloadSigAsync package version downloadDir
        let directoryName = Path.Combine(downloadDir, package)
        if Directory.Exists directoryName
           && (File.GetLastWriteTimeUtc(packageFilename) > Directory.GetLastWriteTimeUtc
                                                               (directoryName)) then
            Directory.Delete(directoryName, true)
        if not (Directory.Exists(directoryName)) then
            extractPackage package version downloadDir
    }

let asyncMap f computation =
    async { let! result = computation
            return f result }

let printError result =
    match result with
    | Choice2Of2 e -> printfn "%O" e
    | _ -> ()

type Configuration() =
    member val public Packages: string array = [||] with get, set
    member val public GnupgKeys: string array = [||] with get, set


let getConfiguration () =
    let configFileName = "epic-updater.toml"
    let executablePath = System.Reflection.Assembly.GetEntryAssembly().Location
                         |> Path.GetDirectoryName
    let configInExecPath = Path.Combine(executablePath, configFileName)
    List.tryFind File.Exists [configFileName; configInExecPath; Path.Combine("/etc", configFileName)]
    |> Option.map (fun x ->
                   printfn "Reading configuration from %s" x
                   Nett.Toml.ReadFile<Configuration> x)
    |> Option.defaultWith (fun () ->
                           let configuration = Configuration()
                           configuration.Packages <- [|"epic"|]
                           configuration.GnupgKeys <- [|"E2D855EB644BB148EC75F44DADCAB4430D49A2C7"|]
                           eprintfn "WARNING: No configuration file found, loading default configuration"
                           configuration)

let main () =
    printfn "Running"
    printfn "Getting configuration"
    getConfiguration()
    |> (fun configuration -> Array.map (processPackage configuration.GnupgKeys) configuration.Packages)
    |> Async.Parallel
    |> Async.Catch
    |> asyncMap printError
    |> Async.RunSynchronously
    printfn "Finished"

let onStart _ =
    printfn "Starting"
    let rec loop() =
        main()
        Thread.Sleep(minutesInterval * 1000 * 60)
        loop()
    loop()
