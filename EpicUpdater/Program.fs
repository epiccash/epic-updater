// Learn more about F# at http://fsharp.org
open System
open System.IO
open System.ServiceProcess
open System.Threading
open ServiceFunctions

#if Windows

// Windows has its own daemon mechanism
type EpicUpdaterService() =
    inherit ServiceBase()
    member this.ServiceName = "Epic Updater"
    override this.OnStart(args : string array) = 
        let t = Thread(ThreadStart(fun () -> onStart args))
        t.Start()
    override this.OnStop() = onStop()

let startService _args =
    use service = new EpicUpdaterService()
    ServiceBase.Run service
#else
// For MacOS and Linux we'll use init systems such as systemd
// They usually terminate the system with a SIGTERM
let startService args =
    // Gracefully exits when receiving a SIGTERM
    AppDomain.CurrentDomain.ProcessExit.Add (fun _ -> onStop ())
    // Gracefully exits when receiving a Ctrl+C
    System.Console.CancelKeyPress.Add (fun _ -> onStop ())
    onStart args
#endif

[<EntryPoint>]
let main argv =
    startService argv
    0
