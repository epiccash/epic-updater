module ServiceFunctions

val onStart: string array -> unit

val onStop: unit -> unit
